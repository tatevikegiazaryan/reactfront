import { ADD_PRODUCT_BASKET, GET_NUMBERS_BASKET, DEC_QUANTITY, INC_QUANTITY, CLEAR_PRODUCTS}  from "../actions/types"
 
const initialState = {
    basketNumber: 0,
    cartCost:0,
    products: [
        {
            id: 0,
            name:'Product_1',
            price:12,
            number:0,
            inCart:false,
        },
         {
            id: 1,
            name:'Product_2',
            price:120,
            number:0,
            inCart:false,
        },
         {
            id: 2,
            name:'Product_3',
            price:1200,
            number:0,
            inCart:false,
        },
         {
            id: 3,
            name:'Product_4',
            price:12000,
            number:0,
            inCart:false,
        }

    ]
    
}

export default (state = initialState, action) => {
    let productSelected = "";
    switch(action.type) {
        case ADD_PRODUCT_BASKET:
            productSelected = { ...state.products[action.payload]}
            console.log(productSelected)
            console.log("sa sa e {...state.products}", {...state.products})
            console.log("sa sa e {...state}", {...state})
            productSelected.number += 1;
            productSelected.inCart = true;
             return  {
                 ...state,
                basketNumber: state.basketNumber + 1,
                cartCost: state.cartCost + state.products[action.payload].price,
                products: state.products.map(item => item.id === action.payload ? productSelected : item)
            }
            case GET_NUMBERS_BASKET:
                return  {
                     ...state
                }
            case INC_QUANTITY:
                productSelected = { ...state.products[action.id]}
                productSelected.number += 1
                return  {
                    ...state,
                   basketNumber: state.basketNumber + 1,
                   cartCost: state.cartCost + state.products[action.id].price,
                   products: state.products.map(item => item.id === action.id ? productSelected : item)
               }
            case DEC_QUANTITY:
                productSelected = { ...state.products[action.id]};
                let newCartCost = 0;
                let newBasketNumber = 0;
                if(productSelected.number === 0) {
                    productSelected.number = 0
                    newCartCost = state.cartCost
                    newBasketNumber = state.basketNumber
                } else {
                    productSelected.number -= 1
                    newCartCost = state.cartCost - state.products[action.id].price
                    newBasketNumber = state.basketNumber - 1
                }
                

                 return  {
                    ...state,
                   basketNumber: newBasketNumber,
                   cartCost: newCartCost, 
                   products: state.products.map(item => item.id === action.id ? productSelected : item)
                }
            case CLEAR_PRODUCTS:
                productSelected = {...state.products[action.payload]};
                let numberBachup = productSelected.number;
                productSelected.number = 0;
                productSelected.inCart = false;
                return {
                    ...state,
                    basketNumber: state.basketNumber - numberBachup,
                    cartCost: state.cartCost - (numberBachup * productSelected.price),
                    products: state.products.map(item => item.id === action.payload ? productSelected : item)
                }
        default:
            return state
    }
}