import axios from 'axios'

export function addPost(post) {
    return {
        type: "ADD_POST",
        payload: new Promise((resolve, reject) => {
            setTimeout(()=> {
                axios.post("http://react-app.test/api/addPost", {
                    'post': post,
                    'likes_count': 12
                })
                .then((response) => {
                    console.log(response.data);
                    return resolve(response.data);
                  })
                  .catch((error) => {
                    console.log(error);
                    return reject(error)
                  })
            }, 2000)
        })
    }
}

