import{createStore, applyMiddleware } from 'redux'
import {composeWithDevTools} from  'redux-devtools-extension'
import rootReducer from './reducers'
import thunk from 'redux-thunk'
// import promise from 'redux-promise-middleware';

const initialState = {};
const middleware = [thunk];
const store = createStore(rootReducer,initialState, composeWithDevTools(applyMiddleware(...middleware)

));

export default store;




