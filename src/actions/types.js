export const ADD_PRODUCT_BASKET = "ADD_PRODUCT_BASKET"
export const GET_NUMBERS_BASKET = "GET_NUMBERS_BASKET"
export const INC_QUANTITY = "INC_QUANTITY"
export const DEC_QUANTITY = "DEC_QUANTITY"
export const CLEAR_PRODUCTS = "CLEAR_PRODUCTS"