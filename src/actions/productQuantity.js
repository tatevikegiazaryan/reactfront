import {INC_QUANTITY, DEC_QUANTITY, CLEAR_PRODUCTS} from './types'

export const productQuantity = (action, id) => {
    return(dispatch) => {
        dispatch ({
            type: action === "inc" ? INC_QUANTITY : DEC_QUANTITY,
            payload:action, id
        })
    }
}

 
export const clearProducts = (id) => {
    return(dispatch) => {
        dispatch ({
            type: CLEAR_PRODUCTS,
            payload: id
        })
    }
}