import React, { useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux';
import { getNumbers } from '../../actions/getAction'
import logo from '../../logo.svg'

const Header = (props) => {

    useEffect(() => {
        console.log('es useEffectn em')
        console.log('Header props', props)
        getNumbers()
    }, [])
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <NavLink className="navbar-brand" to="/">
                        <img src={logo} width="30" height="30" alt="logo" />
                    </NavLink>
                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li className="nav-item" >
                            <NavLink
                                to="/"
                                className="nav-link"
                                exact activeStyle={{ color: 'blue' }}>
                                Home
                    </NavLink>
                        </li>
                        <li className="nav-item" >
                            <NavLink
                                to="/add-product"
                                className="nav-link"
                                exact activeStyle={{ color: 'blue' }}>
                                Add Product
                    </NavLink>
                        </li>
                        {/* <li className="nav-item" >
                            <NavLink
                                to="/"
                                className="nav-link"
                                exact activeStyle={{ color: 'blue' }}>
                                Profile
                    </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink
                                to="/dialogs"
                                className="nav-link"
                                exact activeStyle={{ color: 'blue' }}>
                                Messages
                    </NavLink>
                        </li> */}
                    </ul>
                    <button type="button" className="btn btn-light">
                        <NavLink
                            to="/cart"
                        >
                            <i className="fa fa-cart-plus text-info">

                                my cart  {props.basketProps.basketNumber}</i>
                        </NavLink>
                    </button>
                </div>
            </div>
        </nav>
    )
}

const mapStateToPropss = state => ({
    basketProps: state.basketState
})

export default connect(mapStateToPropss, { getNumbers })(Header);