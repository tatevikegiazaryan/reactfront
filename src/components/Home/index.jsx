import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { addBasket } from '../../actions/addAction'
import style from './Home.module.css'
import prod_1 from '../../images/product_1.jpg'
import prod_2 from '../../images/product_2.jpg'
import prod_3 from '../../images/product_3.jpg'
import prod_4 from '../../images/product_4.jpg'
 
const Home = (props) => {

    return (
        <div className='my-container'>
            <div className={style.image}>
                <img src={prod_1} alt="404" className={`w-100`} />
                <h3>Barev</h3>
                <h3>$15</h3>
                <a className={style.AddToCart} onClick={() => props.addBasket(0)} href="#">Add To Cart</a>
            </div>
            <div className={style.image}>
                <img src={prod_2} alt="404" className={`w-100`} />
                <h3>Barev</h3>
                <h3>$15</h3>
                <a className={style.AddToCart} onClick={() => props.addBasket(1)} href="#">Add To Cart</a>
            </div> <div className={style.image}>
                <img src={prod_3} alt="404" className={`w-100`} />
                <h3>Barev</h3>
                <h3>$15</h3>
                <a className={style.AddToCart} onClick={() => props.addBasket(2)} href="#">Add To Cart</a>
            </div> <div className={style.image}>
                <img src={prod_4} alt="404" className={`w-100`} />
                <h3>Barev</h3>
                <h3>$15</h3>
                <a className={style.AddToCart} onClick={() => props.addBasket(3)} href="#">Add To Cart</a>
            </div>
        </div>
    )
}
export default connect(null, { addBasket })(Home);
