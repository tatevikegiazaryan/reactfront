import React, { Fragment } from "react"
import { connect } from 'react-redux';
import basketReducer from "../../reducers/basketReducer";
import CartItem from './CartItem'


function Cart({ basketProps }) {
    console.log('Cart props', basketProps.products)
    let productsInCart = [];
    basketProps.products.forEach(function (item) {
        if (item.inCart) {
            productsInCart.push(item)
        }
    })

    let empty = <tr>
        <td><h3>Cart is empty</h3></td>
    </tr>

    return (
        <div className="my-container">
            <table className="table table-borderless">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">PRODUCT</th>
                        <th scope="col">PRICE</th>
                        <th scope="col">QUANTITY</th>
                        <th scope="col">TOTAL</th>
                        <th scope="col">DELETE</th>
                    </tr>
                </thead>
                <tbody>
                    {productsInCart.length === 0 ? empty : productsInCart.map((item, index) => <CartItem key={item.id} index={index} cartItem={item} />)}

                    <tr>
                        <td colSpan="3"> </td>
                        <td ><b>Basket Total</b></td>
                        <td><b>${basketProps.cartCost}</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}
const mapStateToPropss = state => ({
    basketProps: state.basketState
})
export default connect(mapStateToPropss)(Cart);