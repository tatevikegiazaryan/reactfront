import React from "react"
import { connect } from 'react-redux';
import style from './style.module.css'
import prod_1 from '../../../images/product_1.jpg'
import prod_2 from '../../../images/product_2.jpg'
import prod_3 from '../../../images/product_3.jpg'
import prod_4 from '../../../images/product_4.jpg'
import {productQuantity, clearProducts} from '../../../actions/productQuantity';
 

function CartItem(props) {
    ///consolum tes ktenas es action uxarkac dispachy
     const productImages = [prod_1, prod_2, prod_3, prod_4];

    return (
        <tr>
            <th scope="row">{props.index + 1}</th>
            <td><img className={style.image} src={productImages[props.cartItem.id]} /><h4>{props.cartItem.name}</h4></td>
            <td>${props.cartItem.price}</td>
            <td>
            <span onClick={() => props.productQuantity("inc", props.cartItem.id)}><b>+ </b></span>
            {props.cartItem.number}
            <span onClick={() => props.productQuantity("dec", props.cartItem.id)}><b> -</b></span>
            </td>
            <td>${props.cartItem.number * props.cartItem.price}</td>
            <td onClick={() => props.clearProducts(props.cartItem.id)}>DELETE</td>
        </tr>
    )
}
const mapDispatchToProps = {
    productQuantity,
    clearProducts,
  }

export default connect(null, mapDispatchToProps)(CartItem)

