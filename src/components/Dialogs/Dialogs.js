import React, { Component } from 'react'
import style from './Dialogs.module.css'
import DialogItem from './DialogItem';
import MessageItem from './MessageItem';
import axios from 'axios'
// import {NavLink} from 'react-router-dom'

export default class Dialogs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };

  }

  componentDidMount() {
    return axios.get("http://react-app.test/api/dialog")
      .then((response) => {
        console.log(response.data);
        this.setState({
          users: response.data
        });
        return response;
      })
      .catch((error) => {
        console.log(error);
        return error;
      })
  }

  render() {
    return (
      <div className={`container`}>
        <div className={`row`}>
          <div className={`col-lg-3`}>
            <div className={`btn-panel btn-panel-conversation`}>
              <a href="" className={`btn  col-lg-6 send-message-btn `} role="button"><i className={`fa fa-search`}></i> Search</a>
              <a href="" className={`btn  col-lg-6  send-message-btn pull-right`} role="button"><i className={`fa fa-plus`}></i> New Message</a>
            </div>
          </div>

          <div className={`col-lg-offset-1 col-lg-7`}>
            <div className={`btn-panel btn-panel-msg`}>

              <a href="" className={`btn  col-lg-3  send-message-btn pull-right`} role="button"><i className={`fa fa-gears`}></i> Settings</a>
            </div>
          </div>
        </div>
        {/* Dialogs */}
        <div className={`row`}>
          <div className={`${style.conversationWrap} col-lg-3`}>

            {this.state.users.map(item => <DialogItem name={item.name} id={item.id} key={item.id} />)}

          </div>

          <div className={`${style.messageWrap} col-8`}>
            <div className={style.msgWrap}>
              {/* Messages */}
              <MessageItem message="Hello Baby" />
              <MessageItem message="Hello " />
            </div>

            <div className={style.sendWrap}>
              <textarea className={`${style.sendMessage} form-control sendMessage`} rows="3" placeholder="Write a reply..."></textarea>

            </div>
            <div className={style.btnPanel}>
              <a href="#" className={`${style.sendMessageBtn}  col-lg-3 btn`} role="button"><i className={`fa fa-cloud-upload`}></i> Add Files</a>
              <a href="#" className={`${style.sendMessageBtn} col-lg-4 text-right btn pull-right`} role="button"><i className={`fa fa-plus`}></i> Send Message</a>
            </div>
          </div>
        </div>

      </div>
    )
  }
}

