import React, { Component } from 'react'
import style from './Post.module.css'

export default class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {};

  }

  render() {
    return (
      <div className="card mt-4 p-4">
        <img src="https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350" />
        <div className="card-body">
          <h5 className="card-title">Card title</h5>
          <p className="card-text">{this.props.message}</p>
          <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
    <button type="button" className="btn btn-primary">Like<span>{this.props.likes_count}</span></button>
        </div>
      </div>
    )
  }
}