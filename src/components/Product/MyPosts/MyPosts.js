import React, { Component } from 'react'
import Post from './Post/Post';
import { connect } from 'react-redux';
import { addPost } from '../../../Redux/actions/addPostAction'

class MyPosts extends Component {
  constructor(props) {
    super(props)
    this.state = {
      text: '',
    };

  }
  handleChange(event) {
    this.state.text = event.target.value
  }

  render() {
    return (
      <div>
        <div><h4 className={`d-flex justify-content-end`}>Post Count {this.props.post.post.length}</h4></div>
        <div className="form-group">
          <textarea className="form-control" rows="3" placeholder="Add Post" onChange={this.handleChange.bind(this)} ></textarea>
        </div>
        <button type="button" onClick={() => this.props.addPost(this.state.text)} className="btn btn-primary">Add Post</button>
        <div>
          {this.props.post.post.map(item => <Post key={item.id} message={item.post} likes_count={item.likes_count} />)}
        </div>
      </div>

    )
  }
}
//aystex state nery karox enq key tal u vercnel ogtagorcel mer componentum
const mapStateToProps = (state) => {
  return {
    post: state.postReducer,
    //sa uxaki vor erkar reduseri annuny cgrem MAthReducer er 
  }
}

const mapDispatchToProps = (dispatch) => ({
  addPost: (post) => { dispatch(addPost(post)) }
})
export default connect(mapStateToProps, mapDispatchToProps)(MyPosts)