import React from 'react';
import { Switch, Route } from "react-router-dom";
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import AddProduct from './components/Product/AddProduct';
import Header from './components/Header';
import Cart from './components/Cart';
import Default from './components/Default';
import Home from './components/Home';
import {Provider} from 'react-redux'
import store from './store'

 

class App extends React.Component {
  constructor(props) {
    super(props);
   }
  render() {
    return ( 
       <Provider store={store}>
         <Header/>
          <Switch>
          {/* <Route exact path="/" component={Profile}></Route>*/}
          <Route exact path="/" render={() => <Home />}></Route>
          <Route exact path="/add-product" render={() => <AddProduct />}></Route>
          <Route exact path="/cart" component={Cart}></Route> 
          {/* <Route exact path="/dialogs" render={() => <Dialogs/> }></Route> */}
          <Route component={Default}></Route>

        </Switch>
        </Provider>
    );
  }
}
 

export default App;
